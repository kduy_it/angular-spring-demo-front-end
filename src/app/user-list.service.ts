import { Injectable, ModuleWithComponentFactories } from '@angular/core';
import { UserService } from './user.service';
import { DepartmentService } from './department.service';
import { User } from './user';
import { Observable, combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import * as _ from 'lodash';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class UserListService {

  constructor(
    private userService: UserService,
    private departmentService: DepartmentService
  ) { }

  loadUserList(): Observable<User[]> {
    return combineLatest(
      [this.departmentService.getDepartmentList(),
      this.userService.getUserList()]
    ).pipe(
      map(([departmentList, userList]) => {
        _.map(userList, user => {
          moment.locale('ja');
          user.birthday = moment(user.birthday).format('LL');
          user.department_name = _.find(departmentList, department => department.id === user.department_id).name;
        });
        return userList;
      })
    );
  }

}
