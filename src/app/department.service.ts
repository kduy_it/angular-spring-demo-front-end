import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Department } from './department';

@Injectable({
  providedIn: 'root'
})
export class DepartmentService {
  private baseURL = 'http://192.168.5.110:9090/api/v1/';
  constructor(
    private httpClient: HttpClient
    ) { }
  
    getDepartmentList(): Observable<Department[]> {
      return this.httpClient.get<Department[]>(`${this.baseURL}department`);
    }

    getDepartmentByID(id: number): Observable<Department> {
      return this.httpClient.get<Department>(`${this.baseURL}department/${id}`);
    }
}
