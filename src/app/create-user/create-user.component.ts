import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from "@angular/forms";
import { User } from '../user';
import { UserService } from '../user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DepartmentService } from '../department.service';
import { Department } from '../department';
import { Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { BsLocaleService } from "ngx-bootstrap/datepicker";
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {
  private locale = 'ja';
  private registerForm = this.fb.group({
    id: [],
    name: ['', Validators.required],
    birthday: ['', Validators.required],
    department_id: ['', Validators.required],
    memo: ['']
  });

  private user: User = {
    id: null,
    name: '',
    birthday: '',
    department_id: null,
    memo: ''
  };

  private departmentList$: Observable<Department[]>;
  private isSubmitted: boolean = false;
  private bsBirthdayDateConfig = { };
  private minDate: Date;

  constructor(
    private userService: UserService,
    private departmentService: DepartmentService,
    private toastrService: ToastrService,
    private bsLocalService: BsLocaleService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.bsLocalService.use(this.locale);
    this.bsBirthdayDateConfig = {
      dateInputFormat: 'LL', 
      containerClass: 'theme-red',
      selectFromOtherMonth: true,
      maxDate: moment().subtract(18, 'years').toDate()
    };
      this.minDate = moment().subtract(18, 'years').toDate();
    this.user.id = this.activeRoute.snapshot.params['id'];
    if (this.user.id) {
      this.loadDataEditForm(this.user.id);
    }
    this.loadDepartmentList();
  }

  private loadDataEditForm(id: number): void {
    let a = this.userService.getUserByID(id).subscribe((user: User) => {
      this.user = user;
      console.log(user);
    }, error => {
      console.error(error);
      this.showErrorToastMessage(error);
    }, () => {
      this.registerForm.controls['department_id'].setValue(this.user.department_id);
      this.registerForm.updateValueAndValidity({ onlySelf: true, emitEvent: true });
    });
  }

  private loadDepartmentList(): void {
    this.departmentList$ = this.departmentService.getDepartmentList();
  }

  private newUser(): void {
    this.isSubmitted = false;
    this.user = new User();
  }

  private save(saveUser: User): void {
    this.userService.createUser(saveUser).subscribe(result => {
      console.log(result);
    }, error => {
        console.error(error);
        this.showErrorToastMessage(error);
    }, () => {
      this.showSuccess('ユーザーが登録する事が成功しました。');
      this.navigateToUserList();
    });
  }

  private update(updateUser: User): void {
    this.userService.updateUser(this.user.id, updateUser).subscribe(result => {
      console.log(result);
    }, error => {
      console.error(error);
      this.showErrorToastMessage(error);
    }, () => {
      this.showSuccess('ユーザーが更新する事が成功しました。')
      this.navigateToUserList();
    })
  }

  private onSubmit(): void {
    this.isSubmitted = true;
    this.registerForm.controls.value;
    let saveUser = new User();
    saveUser.name = this.registerForm.value.name;
    saveUser.birthday = moment(this.registerForm.value.birthday).format('YYYY-MM-DD');
    saveUser.department_id = this.registerForm.value.department_id;
    saveUser.memo = this.registerForm.value.memo;
    if (this.user.id) {
      this.update(saveUser);
    } else {
      this.save(saveUser);
    }
  }

  private navigateToUserList(): void {
    this.router.navigate(['/user']);
  }

  private updateDepartmentValue(e): void {
    this.registerForm.controls['department_id'].setValue(e.target.value, {
      onlySelf: true
    });
    this.registerForm.updateValueAndValidity({ onlySelf: true, emitEvent: true });
  }

  private showSuccess(message: string) {
    this.toastrService.success(message, 'お知らせ', {
      positionClass:　'toast-top-right'
    });
  }

  private showErrorToastMessage(error: any) {
    this.toastrService.error(_.join([_.get(error, 'message', ''), _.get(error, 'error.message', '')], '\n'), 'エラー', {
      positionClass: 'toast-top-right'
    });
  }
}
