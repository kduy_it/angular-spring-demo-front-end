import { Injectable } from '@angular/core';
import { User } from './user';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseURL = 'http://192.168.5.110:9090/api/v1/'
  constructor(
    private httpClient: HttpClient
  ) { }

  getUserList(): Observable<User[]> {
    return this.httpClient.get<User[]>(`${this.baseURL}user/v1`);
  }

  getUserByID(id: number): Observable<any> {
    return this.httpClient.get(`${this.baseURL}user/${id}`);
  }

  createUser(user: User): Observable<Object> {
    return this.httpClient.post(`${this.baseURL}user`, user);
  }

  updateUser(id: number, updateUser: User): Observable<Object> {
    return this.httpClient.put(`${this.baseURL}user/${id}`, updateUser);
  }

  deleteUser(id: number): Observable<any> {
    return this.httpClient.delete(`${this.baseURL}user/${id}`, { responseType: 'text' });
  }
}
