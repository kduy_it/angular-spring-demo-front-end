import { Component, OnInit, OnDestroy, TemplateRef } from '@angular/core';
import { User } from '../user';
import { Observable } from 'rxjs';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { UserListService } from '../user-list.service';
import * as _ from 'lodash';
import { MatDialog } from '@angular/material';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  private userList: Observable<User[]>;
  private dataSource;
  private bsModalRef: BsModalRef;
  private message: string;

  constructor(
    private userService: UserService,
    private userListService: UserListService,
    private bsModalService: BsModalService,
    private toastrService: ToastrService,
    private router: Router
  ) { }

  ngOnInit() {
    this.userList = this.userListService.loadUserList();
    this.dataSource = this.userList;
  }

  ngOnDestroy() {

  }

  private loadData() {
    this.userList = this.userService.getUserList();
  }

  private deleteUserDialog(id: number, name: string, templateRef: TemplateRef<any>) {
    this.message = `ID: ${id} ${name}を削除してもよろしですか？`;
    this.bsModalRef = this.bsModalService.show(templateRef);
  }

  private userDetails(id: number) {
    this.router.navigate(['/detail', id]);
  }

  private createNewUser() {
    this.router.navigate(['/add']);
  }

  private updateUser(id: number) {
    this.router.navigate(['/add', id]);
  }


  public confirm(id: number): void {
    this.userService.deleteUser(id).subscribe(result => {
      console.log(result);
    }, error => {
      console.log(error);
    }, () => {
      this.showSuccess(`ユーザー ID: ${id}が削除しました。`);
      this.loadData();
    })
    this.bsModalRef.hide();
  }

  public decline(): void {
    this.message = 'キャンセル';
    this.bsModalRef.hide();
  }

  private showSuccess(message: string) {
    this.toastrService.success(message, 'お知らせ', {
      positionClass:　'toast-top-right'
    });
  }

}
