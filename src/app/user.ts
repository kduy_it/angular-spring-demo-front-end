export class User {
    id?: number;
    name: string;
    birthday: string;
    department_id: number;
    department_name?: string;
    memo?: string;
}