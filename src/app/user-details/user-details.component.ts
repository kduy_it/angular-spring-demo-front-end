import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {
  private id: number = 0;
  private user: User;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.user = new User();
    
    this.id = this.route.snapshot.params['id'];

    this.userService.getUserByID(this.id).subscribe(result => {
      console.log(result);
      this.user = result;
    }, error => {
      console.log(error)
    }, () => {
    });
  }

  private navigateToUserList() {
    this.router.navigate(['/user']);
  }

}
